const generator = require('openapi3-generator')
const path = require('path')
const { inspect } = require('util')

const red = text => `\x1b[31m${text}\x1b[0m`
const magenta = text => `\x1b[35m${text}\x1b[0m`
const yellow = text => `\x1b[33m${text}\x1b[0m`
const green = text => `\x1b[32m${text}\x1b[0m`

const generate = openapiFile => {
  const config = {
    openapi: path.resolve(process.cwd(), openapiFile),
    base_dir: path.resolve(process.cwd()),
    target_dir: path.resolve(process.cwd(), './generated-src'),
    templates: path.resolve(__dirname, './templates'),
    template: 'koa'
  }

  console.table(config)

  generator
    .generate(config)
    .then(() => {
      console.log(green('Done! ✨'))
      console.log(
        yellow(
          `Check out your shiny new API at ${magenta(
            path.resolve(process.cwd(), './generated-src')
          )}`
        )
      )
    })
    .catch(err => {
      console.error(red('Aaww 💩. Something went wrong:'))
      console.error(
        red(err.stack || err.message || inspect(err, { depth: null }))
      )
    })
}

module.exports = generate
