{{#each openapi.components.schemas}}
{{#unless writeOnly}}
module.exports.{{@key}}In = ({
  {{#each properties}}{{#unless readOnly}}{{@key}}, {{/unless}}{{/each}}
}) => ({
  {{#each properties}}{{#unless readOnly}}{{@key}}, {{/unless}}{{/each}}
})
{{/unless}}
{{/each}}
{{#each openapi.components.schemas}}
{{#unless readOnly}}
module.exports.{{@key}}Out = ({
  {{#each properties}}{{#unless writeOnly}}{{@key}}, {{/unless}}{{/each}}
}) => ({
  {{#each properties}}{{#unless writeOnly}}{{@key}}, {{/unless}}{{/each}}
})
{{/unless}}
{{/each}}
