const Router = require('@koa/router')
const router = new Router()

module.exports = controller => {
{{#each operation}}
  {{#each this.path}}
    {{#validMethod @key}}
  router.{{@key}}('{{../../subresource}}', controller.{{../operationId}})
    {{/validMethod}}
  {{/each}}
{{/each}}

  router.prefix('/{{camelCase operation_name}}')
  return router
}
